import fsp from 'fs/promises';
import fs from 'fs'
import path from 'path'; 
import { URL } from 'url'; 


interface Item {
    id: string, 
    name: string,
    status: boolean
}

class ToDoList{
    list: Item[];

    constructor(){
        this.list =  [];
    }

    init(todos: Item[]){
        this.list = todos;
        this.updateFile();
    }

    add(item: Item){
        this.list.push(item);
        this.updateFile();
    }

    remove(itemid: string){
        this.list = this.list.filter((item:Item) => item.id !== itemid);
        this.updateFile();
    }

    update(itemid: string){
        for (const item of this.list) {
            if(item.id === itemid){
                item.status = true;
            }
        }
        this.updateFile();
    }

    displayAll(){
        console.table(this.list);
    }

    removeAll(){
        this.list = [];
        this.updateFile();
    }
    
    updateFile() {
        const payload = JSON.stringify(this.list,null,2);
        fs.writeFileSync('./todos.json', payload, {encoding:'utf8',flag:'w'})
    }

    displayCompleted(){
        const todosCompleted = this.list.filter((todo) => todo.status === true);
        console.table(todosCompleted);
    }

    displayOpen(){
        const todosOpen = this.list.filter((todo) => todo.status === false);
        console.table(todosOpen);
    }


}

async function init(){
    let myArgs = process.argv.slice(2);
    let myList = new ToDoList();
    if(!fs.existsSync('./todos.json')){
        fs.writeFileSync('./todos.json', JSON.stringify([]))
    } else {
        let data: string = await fsp.readFile('./todos.json', 'utf-8');
        console.log(data);
        let items:Item[] = JSON.parse(data);
        myList.init(items);
    }
    myArgs = process.argv.slice(2);
    let action:string = myArgs[0];
    let itemInfo:string = myArgs[1]; 
    console.log('myArgs: ', myArgs);
    switch(action){
        case 'add':
            let todo:Item = {
                id: Math.random().toString(36).substring(2),
                name:itemInfo,
                status:false
            }
            myList.add(todo);
            break;
        case 'remove':
            myList.remove(itemInfo);
            break;
        case 'update':
            myList.update(itemInfo);
            break;
        case 'display-all':
            myList.displayAll();
            break;
        case 'display-completed':
            myList.displayCompleted();
            break;
        case 'display-open':
            myList.displayOpen();
            break;
        case 'removeAll':
            myList.removeAll();
            break;
        default:
            console.log('invalid action');
    }
}

init();


// var myArgs = process.argv.slice(2);
// console.log('myArgs: ', myArgs);

// switch (myArgs[0]) {
// case 'insult':
//     console.log(myArgs[1], 'smells quite badly.');
//     break;
// case 'compliment':
//     console.log(myArgs[1], 'is really cool.');
//     break;
// default:
//     console.log('Sorry, that is not something I know how to do.');
// }